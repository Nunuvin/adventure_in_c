#include<stdio.h>
#include<stdlib.h>
#include"word_parse.h"


#ifndef VERSIONNUMBER
#define VERSIONNUMBER
    int majorVersion = 0;
    int minorVersion = 0;
    int changeVersion = 1; 
#endif

#ifndef BOOL
#define BOOL
    typedef enum {FALSE=0,TRUE} bool;
#endif

/*
*
* Handles help option in main_menu
*/
void help_dialogue(){
    printf("Welcome to Koma!\n");
    printf("For each WORD you can do help WORD\n");
    printf("For menus enter symbol before bracket\n");
    printf("ie\n1)\n2)\nyou can either enter:\n1\nor\n2\n");
    printf("Otherwise interaction is done as follows:\n");
    printf("[ADJ] SUBJECT VERB [ADJ] OBJECT [MOD]\n");
    printf("[] is optional\n");
    printf("for each type you can list all acceptable words with:\n");
    printf("HELP TYPE\n");
    printf("Press enter to continue\n");
    while(getchar() != '\n');

}

/*
 * handles main_menu
 * first printf options
 * reads in an option
 * handles in switch
 *
 */
int main_menu(){
	enum choices{
		Exit = 1,
		New,
		Continue,
		Settings,
		Help
	};
	bool terminateLoop = FALSE;
    
	do{
		printf("\n");
		printf("Main Menu\n");
		printf("Select one of the options:\n");
		printf("1) Exit\n");
		printf("2) New\n");
		printf("3) Continue\n");
		printf("4) Settings\n");
		printf("5) Help\n");
		
        UserInput UI;
        get_user_input(&UI);

		switch((enum choices)atoi(UI.wordlist[0])){
			case Exit:
				terminateLoop = TRUE;
			break;
			case New:

				terminateLoop = TRUE;
			break;
			case Continue:
                printf("Not implemented\n");
//				terminateLoop = TRUE;
			break;
			
			case Settings:
                printf("Not implemented\n");
//				terminateLoop = TRUE;
			break;
			
			case Help:
                help_dialogue();
			break;
			
			default:
				printf("Please enter a number!\n");		
		}
	
	}while(!terminateLoop);
	return 0;

}


int main() {
	
	printf("WELCOME TO Koma %d.%d.%d\n", majorVersion, minorVersion, changeVersion);
	main_menu();
	return 0;
}
