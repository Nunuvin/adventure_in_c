#include "word_parse.h"
#include <stdio.h>




int get_user_input(UserInput *UI);


/*
*   split string into words based on spaces
*/
int __internal_parse_input(const char *s, UserInput *UI){
    UI->wordcount = 0;
    int wordlen = 0;
    UI->wordlist[0][0]='?';
    while((*s != '\n') && (*s != '\0')){
        switch (*s){
            case ' ':
            case '\t':
                if(UI->wordcount == SENTENCESIZE){
                    return -2;
                }
                if (wordlen == WORDSIZE-1){
                    return -1;
                }
                if(wordlen){
                    UI->wordlist[UI->wordcount][wordlen]='\0';
                    wordlen = 0;
                    UI->wordcount++;
                }
                
            break;
            default:
                if (wordlen == WORDSIZE-1){
                    return -1;
                }
                UI->wordlist[UI->wordcount][wordlen] = *s;
                wordlen++;
        }
        //printf("%s\n",s);
        s++;
    }
    UI->wordcount++;
    return 0;
}

/*
* get user from stdin
*/
int __internal_get_input(char inp[MAXLINESIZE]){
    char c;
    int i = 0;
    while(i < MAXLINESIZE-1){
        c = getchar();
        if(c != '\n'){
            inp[i] = c;
            i++;
        }else{
            break;
        }
        
    }
    inp[i]='\0';
    return 0;
}

void __internal_zero_UserInput(UserInput *UI){
    for (int i=0;i<SENTENCESIZE;i++){
        for(int j=0;j<WORDSIZE;j++){
            UI->wordlist[i][j] = (char) 0;
        }
    }
}

int get_user_input(UserInput *UI){
    char inp[MAXLINESIZE];
    __internal_zero_UserInput(UI);
    __internal_get_input(inp);
    //printf("got it\n");
    return __internal_parse_input(inp, UI);
}

#if defined(WORD_PARSE_DEBUG) || defined(WORD_PARSE_DEBUG_MAIN)

     int __debug_print_userInput(UserInput *UI){
        printf("word #: %d\n", UI->wordcount);
        for(int i=0;i< UI->wordcount;i++){
            printf("%s\n",UI->wordlist[i]);
            printf("---letter by letter parsing:---\n");
            int j=0;
            while(UI->wordlist[i][j] != '\0'){
                printf("%c : %d\n", UI->wordlist[i][j], UI->wordlist[i][j]);
                j++;
            }
            printf("\n");
        }
        return 0;
    }

#endif

#ifdef WORD_PARSE_DEBUG_MAIN

    int main(){
        UserInput UI;
        //int out;

        //out=parse_input("i help desperately me outside!\n", &UI);
        //printf("parsing output: %d\n",out);
        //__debug_print_userInput(&UI);
        get_user_input(&UI);
        __debug_print_userInput(&UI);

        return 0;
    }
#endif