/*
*   Library for parsing user line input. Takes in a line of input
*   from stdin splits on spaces, returns UserInput struct
*/
#ifndef WORD_PARSE_H
#define WORD_PARSE_H

#define SENTENCESIZE 8
#define WORDSIZE 32
#define MAXLINESIZE ((SENTENCESIZE)*(WORDSIZE))


    typedef struct{
        int wordcount;
        char wordlist[SENTENCESIZE][WORDSIZE];
    }UserInput;
  
   
    /*
    *   parses string and splits into words populating UserInput struct
    */
    int get_user_input(UserInput *UI);
#endif
