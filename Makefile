CC = gcc
FLAGS = -Wall -pedantic -Wextra -Werror
OBJ = word_parse.o #char_radix.o

all: word_parse #char_radix
	${CC} main.c ${OBJ} -o ekoma ${FLAGS} 

word_parse:
	${CC} word_parse.c -o word_parse.o ${FLAGS} -c 

char_radix:
	${CC} char_radix.c -o char_radix.o ${FLAGS} -c

word_parse_debug:
	${CC} word_parse.c -o eword_parse ${FLAGS} -DWORD_PARSE_DEBUG_MAIN

char_radix_debug:
	${CC} char_radix.c -o echar_radix ${FLAGS} -DCHAR_RADIX_DEBUG_MAIN

clean:
	rm *.o
