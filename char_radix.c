#include "char_radix.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef BOOL
#define BOOL
    typedef enum {FALSE=0,TRUE=1} bool;
#endif

/*
*
* TODO: Probably issue is with how I pass string 
*   and treat char and string as equals...
*
*/






#if defined(CHAR_RADIX_DEBUG) || defined(CHAR_RADIX_DEBUG_MAIN)
void __debug_print_node(RadixNode *root, bool printChildren);
void __debug_print_radix_tree(RadixNode *root);
#endif



void __internal_set_children(RadixNode *node){
    for(int i=0; i < SYMBOLCOUNT; i++){
        node->children[i] = NULL;
    }
}

/*
*
* convert char to its children symbol number
*
*/
int __internal_convert_to_child_id(char c){
    //#define BIGGESTSYMBOL 127
    //#define SMALLESTSYMBOL 32
    int a = 97;
    int A = 65;
    if(((int) c >= 32) &&((int) c < 97)){
        return (int) c - SMALLESTSYMBOL;
    }else if((int)c > 122){ //symbols after lowercase letters
        return (int) c - DUPLICATESYBOLS - SMALLESTSYMBOL;
    }else{  //lowercase
        return (int) c - a + A - SMALLESTSYMBOL;
    }
}


char __internal_convert_to_upper(char c){
    int a = 97;
    int A = 65;
    if(((int)c > 96)&&((int)c <123)){
       
        return (char)((int) c - a + A); 
    }else
    return c;
}

/*
*
* resets radix node
*
*/
int reset_radix_node(RadixNode *node){
    if(node == NULL){return 1;}
    __internal_set_children(node);
    node->parent = NULL;
    node->value = (char) 0;
    node->id = -1;
    return 0;
}

/*
*
*   Creates Radix node on the heap
*   You can also just make a node on the stack for root
*
*/
RadixNode* create_radix_node(){
    return (RadixNode*) malloc(sizeof(RadixNode));
}

/*
*
*   Delete node created via create_radix_node
*   non recursive in comparison to remove from radix
*
*/
int delete_radix_node(RadixNode *node){
    if (node != NULL){
        free(node);
        return 0;
    }else{
        return 1;
    }
}


int __internal_add_intermediate_node(RadixNode *root, char *str, int id, int sidx){
    if(strlen(str)-sidx==0){
    //LAST NODE    
        root->id = id;
      //printf("last\n");
    }else{
    //Intermediate NODE

         //MAKE INTO FUNC
        RadixNode *child;
        int childid = __internal_convert_to_child_id(str[sidx]);
        if(root->children[childid] == NULL){
            child = create_radix_node();
            reset_radix_node(child);
            
            child->value = __internal_convert_to_upper(str[sidx]);
            child->parent = root;
            root->children[childid] = child;
        }else{
            child = root->children[childid];
        }

        
        //printf("%s \n",str);
        sidx++;
        __internal_add_intermediate_node(child,str,id,sidx);

    }

    return 0;
}



/*
*
* insert string into radix tree. At the last character
* set id to id
* malloc new nodes
* Errors:
* -1 : str length is 0
* -2 : parent is not NULL (not root node...)
*/
int add_to_radix(RadixNode *root, char *str, int id){
        //printf("parent null\n");
        if(strlen(str) == 0){ 
            return -1;
        }else if(root->parent != NULL){
            return -2;
         
        }
        int childid = __internal_convert_to_child_id(str[0]);
        if(root->children[childid] == NULL){
            RadixNode *child = create_radix_node();
            reset_radix_node(child);
            child->value = __internal_convert_to_upper(str[0]);
            child->parent = root;
            root->children[childid] = child;
        }
        
        __internal_add_intermediate_node(root->children[childid],str,id,1);
    
      return 0;
}


/*
*
* remove string from radix tree if present
* ERROR if not found return -1
* cleanup tree, release unused nodes
*/
int remove_from_radix(RadixNode *root, char *str);



int __internal_descent_radix(RadixNode *node, char *str, int sidx){
    if(strlen(str)-sidx==1){
        return node->id;
    }else{
        sidx++;
        
        if(node->children[__internal_convert_to_child_id(str[sidx])] == NULL){
            return -1;
        }else{
            return __internal_descent_radix(node->children[__internal_convert_to_child_id(str[sidx])], str,sidx);
        }
    }
}

/*
*
* returns id of the word (assoc with lowest child id)
* if not found -1
*
*/
int in_radix_id(RadixNode *root, char *str){
    if(root->children[__internal_convert_to_child_id(str[0])] == NULL){
        
        return -1;
    }else{
        return __internal_descent_radix(root->children[__internal_convert_to_child_id(str[0])],str,0);
    }


}



#if defined(CHAR_RADIX_DEBUG) || defined(CHAR_RADIX_DEBUG_MAIN)

 void __debug_print_node(RadixNode *root, bool printChildren){
        printf("----------------------------------\n");
        printf("addr: %p\n",(void*)root);
        printf("id: %d\n", root->id);
        printf("value: %c -> %i\n", root->value, (int) root->value);
        printf("parent: %p\n", (void*) root->parent);
        printf("Chlidren:\n");
        if(printChildren){
            for(int i=32;i<127;i++){
                
                printf("# %c =(char) %d : %p;  ",(char)i,__internal_convert_to_child_id((char)i),(void*)root->children[__internal_convert_to_child_id((char)i)]);
                if((i % 4 == 0) &&(i != 0)){
                    printf("\n");
                }
            }
            printf("\n");
        }

    }

    void __debug_print_radix_tree(RadixNode *root){
        __debug_print_node(root, TRUE);
        for(int i = 0; i < SYMBOLCOUNT; i++){
            if(root->children[i] != NULL){
                __debug_print_radix_tree(root->children[i]);
            }
        }
    }

#endif

#ifdef CHAR_RADIX_DEBUG_MAIN
    int simple_node_test(){

        RadixNode *rn;
        rn = create_radix_node();
        reset_radix_node(rn);
        __debug_print_node(rn, TRUE);
        delete_radix_node(rn);
        //__debug_print_node(rn); //no reliable way to know if freed
        // will give garbage data XD
        return 0;
    }

    int __internal_convert_to_symbol_number_test(){
        printf("a=A is %d f : %d\n",(int)'A',__internal_convert_to_child_id('a'));
        printf("z=Z is %d f : %d\n",(int)'Z',__internal_convert_to_child_id('z'));
        printf("Z is %d f : %d\n",(int)'Z',__internal_convert_to_child_id('Z'));
        printf("~ is %d f : %d\n",(int)'~'-DUPLICATESYBOLS,__internal_convert_to_child_id('~'));
        printf("` is %d f : %d\n",(int)'`', __internal_convert_to_child_id('`'));
        return 0;
    }

    int parts_test(){
        return simple_node_test() ||   __internal_convert_to_symbol_number_test();
    }

    int simple_char_radix_tests(){
        RadixNode *rn;
        rn = create_radix_node();
        reset_radix_node(rn);
        add_to_radix(rn,"A",0);
        __debug_print_radix_tree(rn);
        add_to_radix(rn,"a",1); //will overwrite. should not.
        add_to_radix(rn," ",2);
        __debug_print_radix_tree(rn);
        return 0;
    }

    int complex_char_radix_tests(){
        RadixNode *rn;
        rn = create_radix_node();
        reset_radix_node(rn);
        add_to_radix(rn,"Aab",0);
        //__debug_print_radix_tree(rn);
        add_to_radix(rn,"at",1); //will overwrite. should not.
        add_to_radix(rn,"zoo",2);
        __debug_print_radix_tree(rn);
       
        return 0;
    }

    int find_in_char_radix_tests(){
        RadixNode *rn;
        rn = create_radix_node();
        reset_radix_node(rn);
        add_to_radix(rn,"Aab",0);
        //__debug_print_radix_tree(rn);
        add_to_radix(rn,"at",1); //will overwrite. should not.
        add_to_radix(rn,"zoo",2);
        //__debug_print_radix_tree(rn);
       
        printf("zoo yes: %d\n",in_radix_id(rn,"zoo"));
        printf("moo no: %d\n",in_radix_id(rn,"moo"));
        printf("goo no: %d\n",in_radix_id(rn,"goo"));
        printf("at yes: %d\n",in_radix_id(rn,"at"));
        printf("a no: %d\n",in_radix_id(rn,"a"));
        add_to_radix(rn,"a",3);
        add_to_radix(rn,"goo",4);
        printf("a now yes: %d\n",in_radix_id(rn,"a"));
        printf("goo yes: %d\n",in_radix_id(rn,"goo"));
        return 0;
    }

    int main(){
        find_in_char_radix_tests();

        return 0;
    }

#endif
