/*
*   Radix based word tree. Allows to create radix tree from
*   a string of printable ascii chars
*   last character node can have an id != -1 to show that it
*   is a valid word. You can use all other int vals (except -1)
*   to associate word to some other ds
*
*   To create a valid root node just make one RadixNode. Default
*   settings should suffice.
*
*   Example radix tree with words: a, at, and, be (an is not a word in ex):
*                                   (char)0 (id=-1) // NULL NODE IS ROOT
*                   a (id=0)                 |   b(id=-1)       ...
*           n (id = -1)        t (id = 1)    | e(id=3)
*       d (id = 2)
*
*   Nulls omitted
*   Legend: value (id)
*/

#ifndef CHAR_RADIX_H
#define CHAR_RADIX_H

#include <stdio.h>
#include <stdlib.h>
#define BIGGESTSYMBOL 127
#define SMALLESTSYMBOL 32
#define DUPLICATESYBOLS 26
#define SYMBOLCOUNT BIGGESTSYMBOL - SMALLESTSYMBOL - DUPLICATESYBOLS
#ifndef BOOL
#define BOOL
    typedef enum {FALSE=0,TRUE} bool;
#endif



    typedef struct RadixNode RadixNode;

    struct RadixNode{
        RadixNode *children[SYMBOLCOUNT];
        RadixNode *parent;
        char value;
        int id;

    };

    /*
    *
    *   Creates Radix node on the heap
    *   You can also just make a node on the stack for root
    *
    */
    RadixNode* create_radix_node();


    /*
    *
    *   Delete node created via create_radix_node
    *   non recursive in comparison to remove from radix
    *
    */
    int delete_radix_node(RadixNode *node);

    /*
    *
    * Set RadixNode to its default values
    * All children are NULL
    * parent is null
    * value = 0
    * id = -1
    *
    */
    int reset_radix_node(RadixNode *node);


    /*
    *
    * insert string into radix tree. At the last character
    * set id to id
    * malloc new nodes
    *
    */
    int add_to_radix(RadixNode *root, char *str, int id);


    /*
    * NOT IMPLEMENTED. Current version of project does one time
    * import and does not expect changes to radix tree during process
    *
    *
    *
    * remove string from radix tree if present
    * ERROR if not found return -1
    * cleanup tree, release unused nodes
    * does not remove ROOT NODE. To dispose of it use delete_radix_node
    * if was created via create_radix_node
    */
    int remove_from_radix(RadixNode *root, char *str);


    /*
    *
    * returns id of the word (assoc with lowest child id)
    * if not found -1
    *
    */
    int in_radix_id(RadixNode *root, char *str);


#endif